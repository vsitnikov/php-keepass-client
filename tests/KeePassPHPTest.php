<?php declare(strict_types=1);

/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\ExpectationFailedException as PHPUnitException;
use vsitnikov\KeePassPHPClient\Exceptions\CryptException;
use vsitnikov\KeePassPHPClient\KeePassPHPClient;
use vsitnikov\SharedMemory\AbstractSharedMemoryClient as mem;

/**
 * ETCD class
 *
 * @author    Vyacheslav Sitnikov <vsitnikov@gmail.com>
 * @version   1.0
 * @package   vsitnikov\KeePassPHPClient
 * @copyright Copyright (c) 2019
 */
final class KeePassPHPTest extends TestCase
{
    private $keepass;
    
    public static function setUpBeforeClass(): void
    {
        @unlink(__DIR__."/private.pem");
        @unlink(__DIR__."/public.pem");
        @unlink(__DIR__."/key");
    }
    
    public static function tearDownAfterClass(): void
    {
        @unlink(__DIR__."/private.pem");
        @unlink(__DIR__."/public.pem");
        @unlink(__DIR__."/key");
    }
    
    public function setUp(): void
    {
        if (!file_exists(__DIR__."/private.pem"))
            return;
        
        //  Import the initialized parameters and add them with a deliberately false parameter
        global $global_options;
        $options = $global_options;
        foreach (["database", "password_key", "public_key", "private_key"] as $value)
            $options[$value] = __DIR__."/{$options[$value]}";
        try {
            $this->keepass = new KeePassPHPClient($options);
        } catch (Exception $e) {
            throw new PHPUnitException("Initialization error");
        }
    }
    
    /**
     * Test checkParams function
     *
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CallableException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CryptException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\DatabaseException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\UnexpectedException
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function testCheckParams()
    {
        try {
            $class = new ReflectionClass('\vsitnikov\KeePassPHPClient\KeePassPHPClient');
            $method = $class->getMethod('checkParams');
            $method->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $keepass = new KeePassPHPClient([]);
        $result = $method->invoke($keepass, ["data"]);
        $this->assertIsArray($result);
        $this->assertArrayNotHasKey("data", $result);
        $result = $method->invoke($keepass, ["database"]);
        $this->assertArrayHasKey("database", $result);
        $result = $method->invoke($keepass, ["encode_password"]);
        $this->assertArrayNotHasKey("encode_password", $result);
        $result = $method->invoke($keepass, ["encode_password" => "test"]);
        $this->assertArrayHasKey("encode_password", $result);
    }
    
    /**
     * Test getParams function
     *
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CallableException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CryptException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\DatabaseException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\UnexpectedException
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function testGetParams()
    {
        try {
            $class = new ReflectionClass('\vsitnikov\KeePassPHPClient\KeePassPHPClient');
            $method = $class->getMethod('getParams');
            $method->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $keepass = new KeePassPHPClient([]);
        $result = $method->invoke($keepass);
        $this->assertIsArray($result);
        $this->assertArrayNotHasKey("dummy", $result);
        $this->assertArrayHasKey("database", $result);
    }
    
    /**
     * Test setParams function
     *
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CallableException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CryptException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\DatabaseException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\UnexpectedException
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function testSetParams()
    {
        try {
            $class = new ReflectionClass('\vsitnikov\KeePassPHPClient\KeePassPHPClient');
            $method_get = $class->getMethod('getParams');
            $method_get->setAccessible(true);
            $method_set = $class->getMethod('setParams');
            $method_set->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $keepass = new KeePassPHPClient([]);
        $result = $method_get->invoke($keepass);
        $this->assertArrayNotHasKey("dummy", $result);
        $method_set->invoke($keepass, ["dummy" => "test"]);
        $result = $method_get->invoke($keepass);
        $this->assertArrayHasKey("dummy", $result);
    }
    
    /**
     * Test getParam function
     *
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CallableException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CryptException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\DatabaseException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\UnexpectedException
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function testGetParam()
    {
        try {
            $class = new ReflectionClass('\vsitnikov\KeePassPHPClient\KeePassPHPClient');
            $method = $class->getMethod('getParam');
            $method->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $keepass = new KeePassPHPClient([]);
        $result = $method->invoke($keepass, "dummy");
        $this->assertEmpty($result);
        $result = $method->invoke($keepass, "memory_ttl");
        $this->assertEquals(600, $result);
    }
    
    /**
     * Test setParam function
     *
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CallableException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CryptException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\DatabaseException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\UnexpectedException
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function testSetParam()
    {
        try {
            $class = new ReflectionClass('\vsitnikov\KeePassPHPClient\KeePassPHPClient');
            $method_get = $class->getMethod('getParam');
            $method_get->setAccessible(true);
            $method_set = $class->getMethod('setParam');
            $method_set->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $keepass = new KeePassPHPClient([]);
        $result = $method_get->invoke($keepass, "dummy");
        $this->assertEmpty($result);
        $method_set->invoke($keepass, "dummy", "test");
        $result = $method_get->invoke($keepass, "dummy");
        $this->assertEquals("test", $result);
    }
    
    /**
     * Test issetParam function
     *
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CallableException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CryptException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\DatabaseException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\UnexpectedException
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function testIssetParam()
    {
        try {
            $class = new ReflectionClass('\vsitnikov\KeePassPHPClient\KeePassPHPClient');
            $method = $class->getMethod('issetParam');
            $method->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $keepass = new KeePassPHPClient([]);
        $result = $method->invoke($keepass, "dummy");
        $this->assertFalse($result);
        $result = $method->invoke($keepass, "memory_ttl");
        $this->assertTrue($result);
    }
    
    /**
     * Test unsetParam function
     *
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CallableException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CryptException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\DatabaseException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\UnexpectedException
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function testUnsetParam()
    {
        try {
            $class = new ReflectionClass('\vsitnikov\KeePassPHPClient\KeePassPHPClient');
            $method_get = $class->getMethod('issetParam');
            $method_get->setAccessible(true);
            $method_unset = $class->getMethod('unsetParam');
            $method_unset->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $keepass = new KeePassPHPClient([]);
        $result = $method_get->invoke($keepass, "memory_ttl");
        $this->assertTrue($result);
        $method_unset->invoke($keepass, "memory_ttl");
        $result = $method_get->invoke($keepass, "memory_ttl");
        $this->assertFalse($result);
    }
    
    /**
     * Generate keys and password
     */
    public function testGenerateKeys()
    {
        try {
            $keepass = new KeePassPHPClient([]);
        } catch (Exception $e) {
            throw new PHPUnitException("Initialization error");
        }
        $this->assertInstanceOf('vsitnikov\KeePassPHPClient\KeePassPHPClient', $keepass);
        try {
            $keys = $keepass->generateRSAKeys(4096);
        } catch (CryptException $e) {
            throw new PHPUnitException("Generate keys error");
        }
        $this->assertIsArray($keys);
        $this->assertArrayHasKey("privatekey", $keys);
        $this->assertArrayHasKey("publickey", $keys);
        file_put_contents(__DIR__."/private.pem", $keys['privatekey']);
        file_put_contents(__DIR__."/public.pem", $keys['publickey']);
        $keepass->init(array_combine(["private_key", "public_key"], array_slice($keys, 0, 2)));
        $password = $keepass->encodePass("test");
        $this->assertFalse(ctype_print($password));
        file_put_contents(__DIR__."/key", $password);
    }
    
    /**
     * Test get function
     *
     */
    public function testGet()
    {
        $keepass = $this->keepass;
        $result = $keepass->get("DATA/ORACLE/BIS/MSK/USERNAME");
        $this->assertEquals("username", $result['username']);
        $this->assertEquals("12345", $result['password']);
        $this->assertEquals("BIS_MSK", $result['.metainfo']['tns']);
        $this->assertIsArray($result);
    }
    
    /**
     * Test saveToMemory function
     *
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function testSaveToMemory()
    {
        $keepass = $this->keepass;
        try {
            $class = new ReflectionClass('\vsitnikov\KeePassPHPClient\KeePassPHPClient');
            $method_get = $class->getMethod('getParam');
            $method_get->setAccessible(true);
    
            $reflection = new ReflectionClass($keepass);
            $property = $reflection->getProperty("structure");
            $property->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $database = $method_get->invoke($keepass, "database");
        $memory = mem::new(["memory_key" => $database, "project" => "k", "init_rule" => mem::INIT_DATA_SAFE], true);
        $result = $memory::get("/");
        print_r($database);
        $this->assertNotEmpty($result['value']['keepass']);
        file_put_contents(__DIR__."/keep", $result['value']['keepass']);
        $property->setValue($keepass, "");
        $result = $keepass->saveToMemory();
        $this->assertTrue($result);
        $result = $memory::get("");
        $this->assertEmpty($result['value']['keepass']);
    }
    
    /**
     * Test readFromMemory function
     *
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function testReadFromMemory()
    {
        $keepass = $this->keepass;
        try {
            $class = new ReflectionClass('\vsitnikov\KeePassPHPClient\KeePassPHPClient');
            $method_get = $class->getMethod('getParam');
            $method_get->setAccessible(true);
    
            $reflection = new ReflectionClass($keepass);
            $property = $reflection->getProperty("structure");
            $property->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $structure = $property->getValue($keepass);
        $this->assertEmpty($structure);
        $database = $method_get->invoke($keepass, "database");
        $path = $method_get->invoke($keepass, "memory_path");
        $memory = mem::new(["memory_key" => $database, "project" => "k", "init_rule" => mem::INIT_DATA_SAFE], true);
        $result = $memory::set($path, file_get_contents(__DIR__."/keep"), 100);
        $this->assertTrue($result['result']);
        $result = $memory::get($path);
        $this->assertTrue($result['result']);
        $result = $keepass->readFromMemory();
        $this->assertTrue($result);
        $structure = $property->getValue($keepass);
        $this->assertEquals(file_get_contents(__DIR__."/keep"), $structure);
    }
    
    /**
     * Test cleanMemory function
     *
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function testCleanMemory()
    {
        $keepass = $this->keepass;
        try {
            $class = new ReflectionClass('\vsitnikov\KeePassPHPClient\KeePassPHPClient');
            $method_get = $class->getMethod('getParam');
            $method_get->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $database = $method_get->invoke($keepass, "database");
        $path = $method_get->invoke($keepass, "memory_path");
        $memory = mem::new(["memory_key" => $database, "project" => "k", "init_rule" => mem::INIT_DATA_SAFE], true);
        $result = $memory::get($path);
        $this->assertTrue($result['result']);
        $keepass->cleanMemory();
        $result = $memory::get($path);
        $this->assertFalse($result['result']);
        $this->assertEquals(404, $result['code']);
    }
    
    /**
     * Test openDatabase function
     *
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function testOpenDatabase()
    {
        $keepass = $this->keepass;
        try {
            $class = new ReflectionClass('\vsitnikov\KeePassPHPClient\KeePassPHPClient');
            $method_get = $class->getMethod('getParam');
            $method_get->setAccessible(true);
    
            $reflection = new ReflectionClass($keepass);
            $property = $reflection->getProperty("structure");
            $property->setAccessible(true);
        } catch (ReflectionException $e) {
            throw new PHPUnitException("Reflection error");
        }
        $database = $method_get->invoke($keepass, "database");
        $path = $method_get->invoke($keepass, "memory_path");
        $memory = mem::new(["memory_key" => $database, "project" => "k", "init_rule" => mem::INIT_DATA_SAFE], true);
        $result = $memory::get($path);
        print_r($result);



        return;
        $database = $method_get->invoke($keepass, "database");
        $path = $method_get->invoke($keepass, "memory_path");
        $memory = mem::new(["memory_key" => $database, "project" => "k", "init_rule" => mem::INIT_DATA_SAFE], true);
        $result = $memory::get($path);
        $this->assertTrue($result['result']);
        $keepass->cleanMemory();
        $result = $memory::get($path);
        $this->assertFalse($result['result']);
        $this->assertEquals(404, $result['code']);
    }
    
    ///**
    // * Complex test
    // */
    //public function testComplexTest()
    //{
    //    $keepass = &$this->keepass;
    //    $this->assertTrue($keepass->init);
    //    $result = $keepass->get("DUMMY");
    //    $this->assertEmpty($result);
    //    $result = $keepass->get("DATA/DUMMY");
    //    $this->assertEmpty($result);
    //    $result = $keepass->get("DATA/ORACLE/BIS/MSK/USERNAME");
    //    $this->assertArrayHasKey("username", $result);
    //    $this->assertArrayHasKey("uuid", $result);
    //    $this->assertArrayHasKey("password", $result);
    //    $this->assertArrayHasKey(".metainfo", $result);
    //    $this->assertIsArray($result['.metainfo']);
    //
    //}
}
