<?php declare(strict_types=1);

namespace vsitnikov\KeePassPHPClient;

use KeePassPHP\KeePassPHP;
use Exception as PHPException;
use phpseclib\Crypt\RSA;
use vsitnikov\KeePassPHPClient\Exceptions\CallableException;
use vsitnikov\KeePassPHPClient\Exceptions\CryptException;
use vsitnikov\KeePassPHPClient\Exceptions\DatabaseException;
use vsitnikov\KeePassPHPClient\Exceptions\InitializationException;
use vsitnikov\KeePassPHPClient\Exceptions\UnexpectedException;
use vsitnikov\SharedMemory\AbstractSharedMemoryClient as mem;

class KeePassPHPClient
{
    public const PRIOR_OPEN_MEMORY = "memory";
    public const PRIOR_OPEN_DISK = "disk";
    public const PRIOR_CLOSE_DISK = "disk_no_open";
    
    public const PREFER_FULL_STRUCTURE = "full";
    public const PREFER_RAW_STRUCTURE = "raw";
    
    /** @var bool Successfully open marker */
    public $init = false;
    
    /** @var \KeePassPHP\Database */
    private $db = null;
    
    /** @var array|string */
    private $structure = null;
    
    /** @var \KeePassPHP\CompositeKey Key for open database */
    private $composite_key = null;
    
    /** @var string Password for endcypt/decrypt database in memory */
    private $encode_password = "DefaultPassword";
    
    /** @var int Timestamp of database expired in memory */
    private $memory_expired = null;
    
    /** @var array List of class parameters */
    private $params = null;
    
    /** @var array Default values */
    protected $default_params = [
        "database"         => null,
        "private_key"      => null,
        "public_key"       => null,
        "prior"            => self::PRIOR_OPEN_MEMORY,
        "prefer_structure" => self::PREFER_FULL_STRUCTURE,
        "memory_path"      => "/keepass",
        "memory_ttl"       => 600,
        "before_open"      => null,
        "logger"           => null,
        "debug_level"      => 0,
    ];
    
    /**
     * KeePass constructor.
     *
     * @param mixed ...$init
     *
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CallableException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CryptException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\DatabaseException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\UnexpectedException
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function __construct(...$init)
    {
        $this->init(...$init);
    }
    
    /**
     * @param array $params
     * <pre>
     * Initialization parameters (all optional)
     * string $database        Database file, must be correct file path
     * string $password        Password for open database
     * string $key             Key for open database
     * string $password_key    RSA crypt password
     * string $public_key      Public key for RSA encrypt database password
     * string $private_key     Private key for RSA decrypt database password
     * string $prior           What priority store database, on disk or in shared memory
     * string $encode_password Password for encode KeePass data
     * string $memory_path     Path in shared memory structure
     * string $memory_ttl      Time to live database in shared memory (seconds)
     * string $before_open     Callable method before open database, for example [Foo::class, 'bar'] or [GIT::pull, "path_to_database_repo"]
     * string $logger          Logger interface
     * string $debug_level     Debug level
     * </pre>
     *
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CallableException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CryptException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\DatabaseException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\UnexpectedException
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function init(array $params)
    {
        if (is_null($this->params)) {
            $this->params = $this->default_params;
        }
        //  Parse incoming parameters and move several parameter to class protected variable
        $params = $this->checkParams($params ?? []);
        foreach (['encode_password', 'composite_key'] as $value) {
            if (isset($params[$value])) {
                $this->{$value} = $params[$value];
                unset($params[$value]);
            }
        }
        $this->setParams($params);
        
        //  If set database
        if ($params['prior'] != self::PRIOR_CLOSE_DISK) {
            $this->openDatabase($params);
        } else {
            $this->init = true;
        }
    }
    
    /**
     *  Check parameters
     *
     * @param array $params Parameters, see init() function
     *
     * @return array|false false on fail or array, contains all class properties (internal and getting)
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     * @see KeePass::init()
     */
    protected function checkParams(array $params)
    {
        //  Fill the ONLY EXISTS parameters with the getting values
        $local_vars = $this->getParams();
        foreach ($params as $key => $value) {
            if (array_key_exists($key, $local_vars)) {
                $local_vars[$key] = $value;
            }
        }
        
        //  Check getting parameters
        //  Database parameter, must be valid file path
        if (!is_null($local_vars['database']) && !file_exists($local_vars['database'])) {
            throw new InitializationException('"database" must be valid file path');
        }
        
        //  Database parameter, must be valid file path
        if (!is_null($local_vars['before_open']) && !is_callable($local_vars['before_open'])) {
            throw new InitializationException('"before_open" must be callable');
        }
        
        //  Password for encode database
        if (!is_null($params['encode_password'])) {
            $local_vars['encode_password'] = $params['encode_password'];
        }
        
        //  File or string with public key for decrypt password for database
        if (!is_null($params['public_key'])) {
            $local_vars['public_key'] = file_exists($params['public_key']) ? file_get_contents($params['public_key']) : $params['public_key'];
        }
        
        //  File or string with private key for encrypt password for database
        if (!is_null($params['private_key'])) {
            $local_vars['private_key'] = file_exists($params['private_key']) ? file_get_contents($params['private_key']) : $params['private_key'];
        }
        
        //  File or string with encrypted password for database
        if (!is_null($params['password_key'])) {
            if (is_null($local_vars['public_key']) || is_null($local_vars['private_key'])) {
                throw new InitializationException('No public/private key(s) for encrypted password');
            }
            $params['password_key'] = (ctype_print($params['password_key']) && file_exists($params['password_key'])) ? file_get_contents(
                $params['password_key']
            ) : $params['password_key'];
            $params['password'] = $this->decryptKey($params['password_key'], $local_vars['private_key']);
        }
        
        if (!is_null($params['password'])) {
            $local_vars['composite_key'] = KeePassPHP::masterKey();
            if (!is_null($params['password'])) {
                KeePassPHP::addPassword($local_vars['composite_key'], $params['password']);
            }
            if (!is_null($params['key'])) {
                if (!KeePassPHP::addKeyFile($local_vars['composite_key'], $params['key'])) {
                    throw new InitializationException('Error add key to master key');
                }
            }
        }
        
        return $local_vars;
    }
    
    /**
     *  Get array of all parameters
     *
     * @return array
     */
    protected function getParams(): array
    {
        return $this->params ?? [];
    }
    
    /**
     *  Store all parameters
     *
     * @param array $value Array of all parameters
     *
     * @return array
     */
    protected function setParams(array $value): array
    {
        return $this->params = $value;
    }
    
    /**
     *  Get parameter by name
     *
     * @param string $name Name of parameter
     *
     * @return mixed|null Parameter value or null if parameter not exists
     */
    protected function getParam(string $name)
    {
        if (is_array($this->params) && array_key_exists($name, $this->params ?? [])) {
            return $this->params[$name];
        }
        return null;
    }
    
    /**
     * Set parameter value
     *
     * @param string $name  Parameter name
     * @param mixed  $value Parameter value
     *
     * @return mixed
     */
    protected function setParam(string $name, $value)
    {
        return $this->params[$name] = $value;
    }
    
    /**
     *  Check of parameter exists by name
     *
     * @param string $name Name of parameter
     *
     * @return bool
     */
    protected function issetParam(string $name): bool
    {
        return isset($this->params[$name]);
    }
    
    /**
     * Delete parameter by name
     *
     * @param string $name Name of parameter
     */
    protected function unsetParam(string $name): void
    {
        $this->params[$name] = null;
    }
    
    // TODO: Нужно реализовать какой-нибудь ACL
    
    /**
     *  Get value on path from database
     *
     * @param string $path Path in database
     *
     * @return array
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CallableException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CryptException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\DatabaseException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\UnexpectedException
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function get(string $path): array
    {
        if (!$this->init) {
            throw new DatabaseException('Database not initialized');
        }
        
        //  If data not presens, open database and get this
        if (is_null($this->structure) || $this->memory_expired <= time()) {
            $this->structure = null;
            $this->openDatabase();
        }
        $structure = json_decode($this->getParam("encode_password") != "" ? $this->decrypt($this->structure) : base64_decode($this->structure), true);
        if (json_last_error() != JSON_ERROR_NONE) {
            throw new UnexpectedException('Error to parse database array in JSON format: '.json_last_error_msg());
        }
        
        //  Split path on segments and analyze each
        $path = explode("/", $path);
        for ($i = 0; $i < sizeof($path ?? []); $i++) {
            //  Create new array from current segment
            $structure = $structure[$path[$i]];
        }
        return $structure ?? [];
    }
    
    /**
     *  Write database to shared memory
     *
     * @param array $params [optional] Initialization parameters
     *
     * @return bool Operation result
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\DatabaseException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function saveToMemory(array $params = null): bool
    {
        if (!$this->init) {
            throw new DatabaseException('Database not initialized');
        }
        $params = $this->checkParams($params ?? []);
        
        //  Init shared memory with default parameters
        $memory = mem::new(
            [
                "memory_key" => $params['database'],
                "project"    => $params['prefer_structure'] == self::PREFER_RAW_STRUCTURE ? "r" : "f",
                "init_rule"  => mem::INIT_DATA_SAFE
            ],
            true
        );
        
        //  Write structure to shared memory
        $result = $memory::set($params['memory_path'], $this->structure, $params['memory_ttl']);
        if (!$result['result']) {
            throw new DatabaseException($result['reason']);
        }
        $this->memory_expired = time() + $params['memory_ttl'];
        return true;
    }
    
    /**
     *  Read database from shared memory
     *
     * @param array $params [optional] Initialization parameters
     *
     * @return bool Operation result
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\DatabaseException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function readFromMemory(array $params = null): bool
    {
        $params = $this->checkParams($params ?? []);
        
        //  Init shared memory with default parameters
        $memory = mem::new(
            [
                "memory_key" => $params['database'],
                "project"    => $params['prefer_structure'] == self::PREFER_RAW_STRUCTURE ? "r" : "f",
                "init_rule"  => mem::INIT_DATA_SAFE
            ],
            true
        );
        $result = $memory::get($params['memory_path']);
        if (!$result['result']) {
            throw new DatabaseException('Database not in shared memory');
        }
        if ($result['raw']['metainfo']['expired'] <= time()) {
            return false;
        }
        $this->structure = $result['value'];
        $this->memory_expired = $result['raw']['metainfo']['expired'];
        return true;
    }
    
    /**
     *  Clean shared memory with database
     *
     * @param array $params [optional] Initialization parameters
     *
     * @return bool Operation result
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function cleanMemory(array $params = null): bool
    {
        $params = $this->checkParams($params ?? []);
        
        //  Init shared memory with default parameters
        $memory = mem::new(["memory_key" => $params['database'], "project" => "k", "init_rule" => mem::INIT_DATA_SAFE], true);
        $result = $memory::delete($params['memory_path']);
        return !$result['result'];
    }
    
    /**
     *  Open database and write this to memory, if need
     *
     * @param array $params Initialization parameters
     *
     * @return true
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CallableException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CryptException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\DatabaseException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\UnexpectedException
     * @throws \vsitnikov\SharedMemory\Exceptions\SharedMemoryInitializationException
     */
    public function openDatabase(array $params = null): bool
    {
        $params = $this->checkParams($params ?? []);
        
        //  Read database from memory, if set memory prior
        if ($params['prior'] == self::PRIOR_OPEN_MEMORY) {
            try {
                if (($this->init = $this->readFromMemory())) {
                    return true;
                }
            } catch (PHPException $e) {
            }
        }
        
        if (is_callable($params['before_open'])) {
            if (true !== call_user_func($params['before_open'])) {
                throw new CallableException('Failed to call pre-opening function');
            }
        }
        
        //  Open KeePass database
        $this->db = KeePassPHP::openDatabaseFile($params['database'], $this->composite_key, $error);
        if (is_null($this->db)) {
            throw new DatabaseException('Error to open database');
        }
        
        //  Get database structure to array and encrypt, if needed
        $structure = json_encode($params['prefer_structure'] == self::PREFER_RAW_STRUCTURE ? $this->getRawStructure() : $this->getStructure());
        if (json_last_error() != JSON_ERROR_NONE) {
            throw new UnexpectedException('Error to parse database array in JSON format: '.json_last_error_msg());
        }
        $this->structure = $this->encode_password != "" ? $this->encrypt($structure) : base64_encode($structure);
        $this->init = true;
        
        //  Write database to memory, if set memory prior
        if ($params['prior'] == self::PRIOR_OPEN_MEMORY) {
            $this->saveToMemory();
        }
        
        //  Reset database object and return success
        $this->db = null;
        return $this->init;
    }
    
    /**
     *  Get hierarhy structure KV in array
     *
     * @param \KeePassPHP\Group[]|array|null $groups [optional] Groups
     * @param null|array                     $access [optional] Access list (_allow, _deny, _order, _tns, _server)
     *
     * @return array Current level data
     */
    private function getStructure($groups = null, $access = null)
    {
        $access = $access ?? ["allow" => [], "deny" => [], "order" => [], "tns" => [], "server" => []];
        $groups = $groups ?? $this->db->getGroups();
        $storage = [];
        
        //  If first call ($group is array), fill data for each array value
        if (is_array($groups)) {
            foreach ($groups as $group_id) {
                /** @var $group_id \KeePassPHP\Group */
                $storage[$group_id->name] = $this->getStructure($group_id, $access);
            }
            return $storage;
        }
        
        //  Group contain data
        /** @var $groups \KeePassPHP\Group */
        if ($groups->entries != null) {
            //  Fill current access list from parent access list
            $local_access = [];
            foreach ($access as $access_key => $access_value) {
                $local_access[$access_key] = $access_value[sizeof($access_value) - 1];
            }
            
            // Iterate entries and remember all string field
            foreach ($groups->entries as $entry_id) {
                /** @var \KeePassPHP\Entry $entry_id */
                /** @var \KeePassPHP\UnprotectedString[] $fields */
                $fields = $entry_id->stringFields;
                /** @var string $title */
                $title = $fields['Title']->getPlainString();
                
                foreach ($fields as $key => $value) {
                    /** @var \KeePassPHP\UnprotectedString|\KeePassPHP\ProtectedString $value */
                    
                    if ($key == "Title") {
                        if (mb_substr(trim($title), 0, 1) == "_") {
                            $local_access[mb_substr(mb_strtolower(trim($title)), 1)] = $fields['UserName']->getPlainString();
                            $access[mb_substr(mb_strtolower(trim($title)), 1)][] = $fields['UserName']->getPlainString();
                            continue 2;
                        }
                        continue;
                    }
                    $storage[$title][mb_strtolower($key)] = $value->getPlainString();
                }
                $storage[$title]['uuid'] = $entry_id->uuid;
                $storage[$title]['password'] = $this->db->getPassword($entry_id->uuid);
            }
            foreach ($storage as $key => $value) {
                if (mb_substr(trim($key), 0, 1) != "_") {
                    $storage[$key]['.metainfo'] = $local_access;
                }
            }
        }
        
        //  If there are subgroups in the group, we recursively remember the information for each
        if ($groups->groups != null) {
            foreach ($groups->groups as &$group_id) /** @var $group_id \KeePassPHP\Group */ {
                $storage[$group_id->name] = $this->getStructure($group_id, $access);
            }
        }
        
        //  Return data of current level
        return $storage;
    }
    
    /**
     *  Get raw hierarhy structure KV in array
     *
     * @param \KeePassPHP\Group[]|array|null $groups [optional] Groups
     *
     * @return array Current level data
     */
    private function getRawStructure($groups = null)
    {
        $groups = $groups ?? $this->db->getGroups();
        $storage = [];
        
        //  If first call ($group is array), fill data for each array value
        if (is_array($groups)) {
            foreach ($groups as $group_id) {
                /** @var $group_id \KeePassPHP\Group */
                $storage[$group_id->name] = $this->getRawStructure($group_id);
            }
            return $storage;
        }
        
        //  Group contain data
        /** @var $groups \KeePassPHP\Group */
        if ($groups->entries != null) {
            // Iterate entries and remember all string field
            foreach ($groups->entries as $entry_id) {
                /** @var \KeePassPHP\Entry $entry_id */
                /** @var \KeePassPHP\UnprotectedString[] $fields */
                $fields = $entry_id->stringFields;
                /** @var string $title */
                $title = $fields['Title']->getPlainString();
                
                foreach ($fields as $key => $value) {
                    /** @var \KeePassPHP\UnprotectedString|\KeePassPHP\ProtectedString $value */
                    
                    if ($key == "Title") {
                        continue;
                    }
                    $storage[$title][mb_strtolower($key)] = $value->getPlainString();
                }
                $storage[$title]['password'] = $this->db->getPassword($entry_id->uuid);
            }
        }
        
        //  If there are subgroups in the group, we recursively remember the information for each
        if ($groups->groups != null) {
            foreach ($groups->groups as &$group_id) /** @var $group_id \KeePassPHP\Group */ {
                $storage[$group_id->name] = $this->getRawStructure($group_id);
            }
        }
        
        //  Return data of current level
        return $storage;
    }
    
    /**
     *  Encode text by password
     *
     * @param string      $text     Text for encode
     * @param string|null $password Password for encode
     * @param array       $params   Initialization parameters
     *
     * @return string
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CryptException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     */
    private function encrypt(string $text, ?string $password = null, array $params = null): string
    {
        if (is_null($password)) {
            $params = $this->checkParams($params ?? []);
            $password = $params['encode_password'] ?? $this->encode_password;
            if (is_null($password)) {
                throw new CryptException('Password is not set');
            }
        }
        $iv_length = openssl_cipher_iv_length($cipher = "AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($iv_length);
        $cipher_text_raw = openssl_encrypt($text, $cipher, $password, $options = OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $cipher_text_raw, $password, $as_binary = true);
        $cipher_text = base64_encode($iv.$hmac.$cipher_text_raw);
        return $cipher_text;
    }
    
    /**
     *  Decode text by password
     *
     * @param string      $cipher_text Encoded text
     * @param string|null $password    Password for decode
     * @param array       $params      Initialization parameters
     *
     * @return string|false Result
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CryptException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     */
    private function decrypt(string $cipher_text, ?string $password = null, array $params = null)
    {
        if (is_null($password)) {
            $params = $this->checkParams($params ?? []);
            $password = $params['encode_password'] ?? $this->encode_password;
            if (is_null($password)) {
                throw new CryptException('Password is not set');
            }
        }
        $c = base64_decode($cipher_text);
        $iv_length = openssl_cipher_iv_length($cipher = "AES-128-CBC");
        $iv = substr($c, 0, $iv_length);
        $hmac = substr($c, $iv_length, $sha2len = 32);
        $cipher_text_raw = substr($c, $iv_length + $sha2len);
        $plain_text = openssl_decrypt($cipher_text_raw, $cipher, $password, $options = OPENSSL_RAW_DATA, $iv);
        $calc_mac = hash_hmac('sha256', $cipher_text_raw, $password, $as_binary = true);
        return hash_equals($hmac, $calc_mac) ? $plain_text : false;
    }
    
    /**
     *  Encode data with RSA public key
     *
     * @param string $plaintext  Data for encrypt
     * @param string $public_key [optional] Public key, if not set, then get from default public_key from class
     *
     * @return string Encrypted data
     */
    private function cryptKey($plaintext, $public_key = null): string
    {
        $public_key = $public_key ?? $this->getParam("public_key");
        $rsa = new RSA();
        $rsa->loadKey($public_key);
        return $rsa->encrypt($plaintext);
    }
    
    /**
     *  Dencode data with RSA private key
     *
     * @param string $ciphertext  Encrypted data
     * @param string $private_key [optional] Private key, if not set, then get from default private_key from class
     *
     * @return string Decrypted data
     */
    private function decryptKey($ciphertext, $private_key = null): string
    {
        $private_key = $private_key ?? $this->getParam("private_key");
        $rsa = new RSA();
        $rsa->loadKey($private_key);
        return $rsa->decrypt($ciphertext);
    }
    
    /**
     * @param string|null $key
     * @param array       $params
     *
     * @return string
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CryptException
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\InitializationException
     */
    public function encodePass(string $key = null, array $params = null): string
    {
        $params = $this->checkParams($params ?? []);
        if (is_null($params['public_key'])) {
            throw new CryptException('Public key is not set');
        }
        $key = $key ?? bin2hex(openssl_random_pseudo_bytes(64));
        return $this->cryptKey($key, $params['public_key']);
    }
    
    /**
     *  Create RSA keys pair
     *
     * @param int $size [optional 1024] Length keys in byt
     *
     * @return array Result array with keys: "privatekey" and "publickey"
     * @throws \vsitnikov\KeePassPHPClient\Exceptions\CryptException
     */
    public function generateRSAKeys(int $size = null): array
    {
        $size = $size ?? 1024;
        if (!is_int($size) || $size < 1024) {
            throw new CryptException('Size bust be 1024 bits or more');
        }
        $rsa = new RSA();
        $rsa->setPublicKeyFormat(RSA::PUBLIC_FORMAT_OPENSSH);
        return $rsa->createKey($size);
    }
    
}