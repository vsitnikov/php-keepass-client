<?php

namespace vsitnikov\KeePassPHPClient\Exceptions;

/**
 * Class DatabaseException
 *
 * @package vsitnikov\KeePassPHPClient\Exceptions
 */
class DatabaseException extends Exception
{
}
