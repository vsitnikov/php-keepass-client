<?php

namespace vsitnikov\KeePassPHPClient\Exceptions;

/**
 * Class CallableException
 *
 * @package vsitnikov\KeePassPHPClient\Exceptions
 */
class CallableException extends Exception
{
}
