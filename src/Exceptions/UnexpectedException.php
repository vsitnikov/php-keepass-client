<?php

namespace vsitnikov\KeePassPHPClient\Exceptions;

/**
 * Class UnexpectedException
 *
 * @package vsitnikov\KeePassPHPClient\Exceptions
 */
class UnexpectedException extends Exception
{
}
