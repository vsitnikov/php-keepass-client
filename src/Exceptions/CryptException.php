<?php

namespace vsitnikov\KeePassPHPClient\Exceptions;

/**
 * Class CryptException
 *
 * @package vsitnikov\KeePassPHPClient\Exceptions
 */
class CryptException extends Exception
{
}
