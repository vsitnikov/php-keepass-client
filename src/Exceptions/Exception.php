<?php

namespace vsitnikov\KeePassPHPClient\Exceptions;

use Exception as phpException;

/**
 * Class KeePassPHPClientException
 *
 * @package vsitnikov\KeePassPHPClient\Exceptions
 */
class Exception extends phpException
{
}
