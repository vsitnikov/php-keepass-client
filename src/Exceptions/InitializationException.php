<?php

namespace vsitnikov\KeePassPHPClient\Exceptions;

/**
 * Class InitializationException
 *
 * @package vsitnikov\KeePassPHPClient\Exceptions
 */
class InitializationException extends Exception
{
}
